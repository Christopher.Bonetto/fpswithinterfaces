﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolBulletActions : MonoBehaviour
{
    public int pistolBulletDamage;
    private IMovement typeOfMovement;
    public Transform spawnPoint;

    private int effectiveDamage;
    

    // Start is called before the first frame update
    void Start()
    {
        typeOfMovement = new LinearMovement();
        Destroy(this.gameObject, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        typeOfMovement.Move(gameObject, spawnPoint);
    }

    private void OnCollisionEnter(Collision collision)
    {
        IDamageable hitted = collision.gameObject.GetComponent<IDamageable>() as IDamageable;
        
        if (hitted != null && collision.gameObject.GetComponent<NormalEnemiesActions>())
        {
            hitted.TakeDirectDamage(pistolBulletDamage);
            Destroy(gameObject);
        }
        else if (hitted != null && collision.gameObject.GetComponent<ArmoredEnemiesActions>())
        {
            hitted.TakeDirectDamage(pistolBulletDamage);
            Destroy(gameObject);
        }

        //IDamageableButWithArmor hittedArmored = collision.gameObject.GetComponent<IDamageableButWithArmor>() as IDamageableButWithArmor;

        //if (hittedArmored != null && collision.gameObject.GetComponent<ArmoredEnemiesActions>())
        //{
            
            
        //    effectiveDamage = hittedArmored.TakeDamageWithArmor(pistolBulletDamage, armatureFinded);

            

        //    Destroy(gameObject);
        //}

    }
}

