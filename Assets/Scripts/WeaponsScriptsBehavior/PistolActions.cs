﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolActions : MonoBehaviour, IPickableWeapons
{
    public IShoot pistolTypeShoot;

    public float firePistolRate;
    public int maxPistolAmmo;
    public bool canShoot;

    public GameObject pistolBullet;
    
    
    // Start is called before the first frame update
    void Start()
    {
        pistolTypeShoot = new ShootingType();
        
    }


    public bool PistolShoot()
    {
        if(pistolTypeShoot.InterfaceShoot(pistolBullet, firePistolRate, gameObject))
        {
            return true;
        }
        return false;
    }

    public float NewWeaponChangeFireRate(float fireWeaponDelay)
    {
        fireWeaponDelay = firePistolRate;
        return fireWeaponDelay;        
    }

    public int NewWeaponChangeAmmo(int ammoToChange)
    {
        ammoToChange = maxPistolAmmo;
        return ammoToChange;
    }
}
