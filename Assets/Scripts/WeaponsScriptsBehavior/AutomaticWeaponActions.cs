﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticWeaponActions : MonoBehaviour, IPickableWeapons
{
    public IShoot autoWeaponTypeShoot;

    public float fireAutoRate;
    public int maxAutoAmmo;
    public bool canShoot;

    public GameObject autoWeaponBullet;


    // Start is called before the first frame update
    void Start()
    {
        autoWeaponTypeShoot = new ShootingType();

    }


    public bool AutoShoot()
    {
        if (autoWeaponTypeShoot.InterfaceShoot(autoWeaponBullet, fireAutoRate, gameObject))
        {
            return true;
        }
        return false;
    }

    public float NewWeaponChangeFireRate(float fireWeaponDelay)
    {
        fireWeaponDelay = fireAutoRate;
        return fireWeaponDelay;
    }

    public int NewWeaponChangeAmmo(int ammoToChange)
    {
        ammoToChange = maxAutoAmmo;
        return ammoToChange;
    }
}
