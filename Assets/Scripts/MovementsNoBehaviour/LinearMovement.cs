﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearMovement : IMovement
{
    public void Move(GameObject go, Transform spawnPoint)
    {
        go.transform.Translate(spawnPoint.transform.forward * 10 * Time.deltaTime);
    }

   
}
