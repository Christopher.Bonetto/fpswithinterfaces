﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingType : MonoBehaviour, IShoot
{
    float timeToLastShoot;


    public bool InterfaceShoot(GameObject bullet, float delay, GameObject bulletSpawnPoint)
    {
        
        if(Time.time >= delay + timeToLastShoot)
        {
            GameObject temporaryBullet = Instantiate(bullet, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation) as GameObject;
            temporaryBullet.GetComponent<PistolBulletActions>();
            
            timeToLastShoot = Time.time;
            return true;
        }
        return false;
        
    }

    
}
