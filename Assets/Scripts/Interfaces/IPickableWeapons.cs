﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickableWeapons
{
    float NewWeaponChangeFireRate(float fireWeaponDelay);
    int NewWeaponChangeAmmo(int ammoToChange);
}
