﻿public interface IDamageable
{
    void TakeDirectDamage(int damage);
}

public interface IDamageableWithArmor
{
    int TakeDamageWithArmor(int damage, int armor);
}
