﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShoot
{
    bool InterfaceShoot(GameObject bullet, float delay, GameObject bulletSpawnPoint);
    
}
