﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalEnemiesActions : MonoBehaviour, IDamageable
{
    
    public int normalEnemyHp;

    public void TakeDirectDamage(int damage)
    {
        normalEnemyHp -= damage;
        if(normalEnemyHp <= 0)
        {
            Destroy(this.gameObject);
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void Update()
    {
        
    }
}
