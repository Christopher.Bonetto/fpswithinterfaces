﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmoredEnemiesActions : MonoBehaviour, IDamageable
{
    public IDamageable typeOfDamageTaken;
    public int armoredEnemyArmature;
    public int armoredEnemyHp;
    public int damageTaken;

    public GameObject pistolPrefab;
    public Transform enemySpawnPoint;
    public PistolActions pistolScriptReference;

    public float enemyFireRate;
    private float temporaryEnemyFireRate;
    private int enemyMaxAmmo;
    public int currentAmmo;

    private int temporaryIncreaseValue;

    public float ViewRadius;

    [Range(0, 360)]
    public float ViewAngle;

    public LayerMask DetectionMask;

    private Collider[] m_DetectedColliders = new Collider[10];

    private int m_NumberOfCollisons;

    private Vector3 PlayerPosition;

    private bool bPlayerDetected;


    // Start is called before the first frame update
    void Start()
    {
        InstantiateWeapon();
        bPlayerDetected = false;

    }

    // Update is called once per frame
    void Update()
    {
        
        DetectEntity();
    }


    private void InstantiateWeapon()
    {
        GameObject Pistol = Instantiate(pistolPrefab, enemySpawnPoint.transform.position, enemySpawnPoint.rotation, enemySpawnPoint) as GameObject;
        Pistol.transform.SetParent(enemySpawnPoint.transform);
        pistolScriptReference = Pistol.GetComponent<PistolActions>();

        enemyFireRate = pistolScriptReference.NewWeaponChangeFireRate(temporaryEnemyFireRate);
        enemyMaxAmmo = pistolScriptReference.NewWeaponChangeAmmo(temporaryIncreaseValue);
        
        temporaryIncreaseValue = 0;
        temporaryEnemyFireRate = 0;
    }

    private void EnemyShoot()
    {
        if (currentAmmo > 0 && pistolScriptReference.PistolShoot())
        {
            pistolScriptReference.PistolShoot();
            currentAmmo--;

        }
    }


    public void DetectEntity()
    {

        m_NumberOfCollisons = Physics.OverlapSphereNonAlloc(transform.position, ViewRadius, m_DetectedColliders, DetectionMask);
        for (int i = 0; i < m_NumberOfCollisons; i++)
        {
            if (m_DetectedColliders[i])
            {
                float angle = Vector3.Angle(transform.forward, m_DetectedColliders[i].transform.position - transform.position);
                
                if (Mathf.Abs(angle) < ViewAngle / 2)
                {
                    PlayerPosition = m_DetectedColliders[i].transform.position;
                    Debug.Log("Player Detected");
                    bPlayerDetected = true;
                    EnemyShoot();
                }
                else
                {
                    bPlayerDetected = false;
                }
            }
        }

        if (m_NumberOfCollisons == 0)
        {
            bPlayerDetected = false;
        }

        if (!bPlayerDetected)
        {
            Debug.Log("No one see");
        }
    }



    public void TakeDirectDamage(int damage)
    {
        if(damage > armoredEnemyArmature)
        {
            damage -= armoredEnemyArmature;
            armoredEnemyHp -= damage;
        }
        else
        {
            armoredEnemyHp--;
        }

        if(armoredEnemyHp <= 0)
        {
            Destroy(this.gameObject);
        }
    }


#if UNITY_EDITOR

    private void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);
    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.green, ViewAngle, ViewRadius);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }
#endif


}
