﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoMunitionsActions : MonoBehaviour, IPickable
{
    public int valueToIncrease;

    public int increaseValue(int amountToIncrease)
    {
        amountToIncrease = valueToIncrease;
        return amountToIncrease;

    }
}
